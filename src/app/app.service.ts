import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import {environment} from 'src/environments/environment';
import {GeneralService} from './general.service';
import {catchError, map, switchMap} from 'rxjs';
import {AuthInterceptor} from './interceptors/auth.interceptor';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  api: string = environment.api_url;

  constructor(
    private http: HttpClient,
    private globalService: GeneralService
  ) {
  }

  login(model: any) {
   return this.globalService.LoadPostAPI('auth/admin-login', model).pipe(
      map((data) => {

        const userData = JSON.parse(data);
        AuthInterceptor.accessToken = userData.data.access_token;

        if (userData.status_code == 200) {
          sessionStorage.setItem('token', userData.data.access_token);
          sessionStorage.setItem('access_expires_at', userData.data.access_expires_at);
          sessionStorage.setItem('refresh_token', userData.data.refresh_token);
          sessionStorage.setItem('refresh_expires_at', userData.data.refresh_expires_at);
          sessionStorage.setItem('user', JSON.stringify(userData.data.me));

          return {'status': 200};
        } else {
          return {'status': 403};
        }
      }),
      catchError((err) => {
        throw err;
      })
    )

  }


  update(model: any) {
    return this.globalService.LoadPutAPI('auth/update', model).pipe(
      map((res) => {
        const result = JSON.parse(res)
        const updateUser = result.data.me
        return {data: updateUser, 'status': 200};
      }),
      catchError((err) => {
        throw err;
      })
    )
  }


  get_prof() {
    return this.globalService.LoadGetAPI('me', {})
  }

  logout() {
    return this.globalService.refreshApi('auth/logout', {access_token: sessionStorage.getItem('token')})
  }

  refresh(data: any) {
    return this.globalService.refreshApi('auth/refresh', data).pipe(
      map((res: any) => {
        AuthInterceptor.accessToken = res.data.access_token;
        return res
      }),
      catchError((err) => {
        throw err;
      })
    )

  }
}
