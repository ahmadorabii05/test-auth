import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {AppService} from '../app.service';
import {User} from '../model/user';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  title: string = "Edit Profile";
  @Input()
  userData: User = new User;
  msg: any;

  constructor(private titleService: Title, private authService: AppService, private router: Router) {}

  form = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)])
  });


  get f() {
    return this.form.controls;
  }

  submit() {
    this.authService.update(this.form.value).subscribe(
      data => {
        if (data && data.status == 200) {
          this.router.navigate(['/home']);
        }
      },
      err => {
        this.msg = err;
      },
      () => console.log('success')
    )
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    if (!sessionStorage['token']) {
      this.router.navigate(['/login']);
    }
  }

}
