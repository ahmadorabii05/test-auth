import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  title: string = "Login";

  msg = '';

  constructor(
    private titleService: Title,
    private authService: AppService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    if (sessionStorage['token']) {
      this.router.navigate(['home']);
    }
  }

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required)
  });

  get f() {
    return this.form.controls;
  }

  submit() {
    this.authService.login(this.form.value).subscribe(
      data => {
        if (data && data.status == 200) {
          this.router.navigate(['/home']);
        }
      },
      err => {
        this.msg = err;
      },
      () => console.log('success')
    )
  }

}

