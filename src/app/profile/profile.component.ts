import {Component, Input, OnInit} from '@angular/core';
import {AppService} from '../app.service';
import {User} from '../model/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  @Input()
  userData: User = new User;

  constructor(private authService: AppService) {}

  ngOnInit(): void {
    this.authService.get_prof().subscribe((res) => {
      const data = JSON.parse(res);
      this.userData = data.data;
    })
  }
}
