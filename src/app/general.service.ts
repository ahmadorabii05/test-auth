import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {


  /**Environment Variable **/
  public api_url: string = environment.api_url;

  public headers = new HttpHeaders();

  constructor(
    private http: HttpClient
  ) {
    this.headers = this.getHeaders();
  }


  getHeaders(): HttpHeaders {
    return new HttpHeaders()
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Methods', environment.methods)
      .set('Access-Control-Allow-Headers', environment.allow_headers)
      .set('Access-Control-Allow-Credentials', environment.allow_credentials)
      .set('App-Secret', environment.app_secret)
      .set('Platform', environment.platform)
      .set('Accept', environment.accept)
      .set('Authorization', 'Bearer ' + sessionStorage.getItem('refresh_token'));
  }


  public LoadGetAPI(API: string, data: any) {
    return this.http
      .get(this.api_url + API, {
        headers: this.headers,
        params: data,
        // TODO this solution for refresh token but ,
        // TODO I faced problem CORS when you switch project on your server the problem will solved !.
        // withCredentials: true
      })
      .pipe(map((data) => {
          return JSON.stringify(data);
        }),
        catchError((err) => {
            throw err;
          }
        ));
  }


  public LoadPostAPI(API: string, data: any) {
    return this.http
      .post(this.api_url + API, data, {
        headers: this.headers,
        // TODO this solution for refresh token but ,
        // TODO I faced problem CORS when you switch project on your server the problem will solved !.
        // withCredentials: true
      })
      .pipe(
        map((data) => {
          return JSON.stringify(data);
        }),
        catchError((err) => {
            throw err.error.message;
          }
        )
      )
  }

  public LoadPutAPI(API: string, data: any) {
    return this.http
      .put(this.api_url + API, data, {
        headers: this.headers,
        // TODO this solution for refresh token but ,
        // TODO I faced problem CORS when you switch project on your server the problem will solved !.
        // withCredentials: true
      })
      .pipe(
        map((res) => {
          return JSON.stringify(res);
        }),
        catchError((err) => {
            throw err.error.message;
          }
        )
      )
  }

  public refreshApi(API: string, data: any) {
    return this.http.get(this.api_url + API, {
      headers: this.headers,
      params: data,
      // TODO this solution for refresh token but ,
      // TODO I faced problem CORS when you switch project on your server the problem will solved !.
      // withCredentials: true
    })
      .pipe(map((data) => {
          return JSON.stringify(data);
        }),
        catchError((err) => {
            throw `error : ${err.error.message}`;
          }
        ));
  }


}
