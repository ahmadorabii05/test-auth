import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AppService} from '../app.service';
import {User} from '../model/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @Input() userData: User = new User;

  page: string = 'page';

  constructor(private router: Router, private activeRoute: ActivatedRoute, private authService: AppService) {
  }

  updateData(data: User) {
    this.userData = data;
  }

  ngOnInit(): void {
    if (!sessionStorage['token']) {
      this.router.navigate(['login']);
    }


    this.authService.get_prof().subscribe((res) => {
      const data = JSON.parse(res);
      this.userData = data.data;
    })
    if (this.activeRoute.snapshot.params['page']) {
      this.page = this.activeRoute.snapshot.params['page'];
    } else {
      this.page = 'page';
    }
  }

}
