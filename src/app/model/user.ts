export class User {
    id!: number
    email!: string
    has_verified_email!: boolean
    phone!: string
    has_verified_phone!: boolean
    name!: string
    image!: string
}
   