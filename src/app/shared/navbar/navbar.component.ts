import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { AuthInterceptor } from 'src/app/interceptors/auth.interceptor';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  // userData: User = new User;
  // userName :String ='';
  constructor(private authService: AppService, private router: Router) { }

  logout() {
    // this.authService.logout().subscribe(()=>{
    //   AuthInterceptor.accessToken ='';
    //   this.router.navigate(['/'])
    // })
    sessionStorage.clear();
    window.location.href = '/';
  }

  ngOnInit(): void {
    // if(sessionStorage.getItem('user')){
    //   this.userData = JSON.parse(sessionStorage.getItem('user')!);
    //   this.userName = this.userData.name;
    // }
  }

}
