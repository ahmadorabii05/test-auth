// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api_url: 'https://phplaravel-718120-2386003.cloudwaysapps.com/api/v1/',
  app_secret: '*(3%13@Uh@1',
  platform: 'web',
  accept: 'application/json',
  allow_credentials :"true",
  allow_headers :'X-Requested-With, content-type',
  methods :'GET, POST, PUT',

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
